package ru.tsc.apozdnov.tm.api.repository;

import ru.tsc.apozdnov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

}