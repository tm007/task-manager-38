package ru.tsc.apozdnov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.api.endpoint.*;
import ru.tsc.apozdnov.tm.api.service.*;
import ru.tsc.apozdnov.tm.endpoint.*;
import ru.tsc.apozdnov.tm.service.*;
import ru.tsc.apozdnov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;

public class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();


    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final Backup backup = new Backup(this);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(projectTaskEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
    }

    @SneakyThrows
    private void initDataTables() {
        Connection connection = connectionService.getConnection();
        final String sqlDropTableTask = "DROP TABLE IF EXISTS tm_task";
        connection.createStatement().executeUpdate(sqlDropTableTask);
        final String sqlCreateTableTask = "CREATE TABLE tm_task (" +
                "id char(36) NOT NULL, " +
                "name varchar(255) DEFAULT NULL, " +
                "description varchar(255) DEFAULT NULL, " +
                "status varchar(255) DEFAULT NULL, " +
                "user_id varchar(255) DEFAULT NULL, " +
                "created timestamp without time zone DEFAULT now(), " +
                "project_id varchar(255) DEFAULT NULL, " +
                "date_end TIMESTAMP DEFAULT NULL, " +
                "date_begin TIMESTAMP DEFAULT NULL, " +
                "PRIMARY KEY (id) " +
                ")";
        connection.createStatement().executeUpdate(sqlCreateTableTask);

        final String sqlDropTableProject = "DROP TABLE IF EXISTS tm_project";
        connection.createStatement().executeUpdate(sqlDropTableProject);
        final String sqlCreateTableProject = "CREATE TABLE tm_project (" +
                "id char(36) NOT NULL, " +
                "name varchar(255) DEFAULT NULL, " +
                "description varchar(255) DEFAULT NULL, " +
                "status varchar(255) DEFAULT NULL, " +
                "user_id varchar(255) DEFAULT NULL, " +
                "created timestamp without time zone DEFAULT now(), " +
                "PRIMARY KEY (id) " +
                ")";
        connection.createStatement().executeUpdate(sqlCreateTableProject);

        final String sqlDropTableUser = "DROP TABLE IF EXISTS tm_user";
        connection.createStatement().executeUpdate(sqlDropTableUser);
        final String sqlCreateTableUser = "CREATE TABLE tm_user (" +
                "id char(36) NOT NULL, " +
                "login varchar(255) NOT NULL, " +
                "password_hash varchar(255) NOT NULL, " +
                "email varchar(255) DEFAULT NULL, " +
                "first_name varchar(255) DEFAULT NULL, " +
                "middle_name varchar(255) DEFAULT NULL, " +
                "last_name varchar(255) DEFAULT NULL, " +
                "role varchar(255) DEFAULT NULL, " +
                "lock boolean DEFAULT false, " +
                "PRIMARY KEY (id) " +
                ")";
        connection.createStatement().executeUpdate(sqlCreateTableUser);
        connection.commit();
        connection.close();
    }


    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        backup.stop();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void run() {
        initPID();
        initDataTables();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
    }

}