//package ru.tsc.apozdnov.tm.endpoint;
//
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.experimental.categories.Category;
//import ru.tsc.apozdnov.tm.api.endpoint.IAuthEndpoint;
//import ru.tsc.apozdnov.tm.api.endpoint.ITaskEndpoint;
//import ru.tsc.apozdnov.tm.api.service.IPropertyService;
//import ru.tsc.apozdnov.tm.dto.request.*;
//import ru.tsc.apozdnov.tm.dto.response.*;
//import ru.tsc.apozdnov.tm.enumerated.Status;
//import ru.tsc.apozdnov.tm.marker.ISoapCategory;
//import ru.tsc.apozdnov.tm.model.Task;
//import ru.tsc.apozdnov.tm.service.PropertyService;
//import ru.tsc.apozdnov.tm.util.DateUtil;
//
//import java.util.Date;
//
//@Category(ISoapCategory.class)
//public final class TaskEndpointTest {
//
//    @NotNull
//    private final IPropertyService propertyService = new PropertyService();
//
//    @NotNull
//    private final String host = propertyService.getServerHost();
//
//    @NotNull
//    private final String port = Integer.toString(propertyService.getServerPort());
//
//    @NotNull
//    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(host, port);
//
//    @NotNull
//    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(host, port);
//
//    @Nullable
//    private String token;
//
//    @Nullable
//    private Task taskBefore;
//
//    @Before
//    public void init() {
//        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(new UserLoginRequest("user", "user"));
//        token = loginResponse.getToken();
//        taskEndpoint.clearTask(new TaskClearRequest(token));
//        @NotNull final TaskCreateResponse createResponse = taskEndpoint.createTask(
//                new TaskCreateRequest(token, "test", "test", null, null)
//        );
//        taskBefore = createResponse.getTask();
//    }
//
//    @Test
//    public void changeTaskStatusById() {
//        Assert.assertThrows(Exception.class,
//                () -> taskEndpoint.changeTaskStatusById(
//                        new TaskChangeStatusByIdRequest(null, taskBefore.getId(), Status.IN_PROGRESS)));
//        Assert.assertThrows(Exception.class,
//                () -> taskEndpoint.changeTaskStatusById(
//                        new TaskChangeStatusByIdRequest(token, taskBefore.getId(), null)));
//        TaskChangeStatusByIdResponse response = taskEndpoint.changeTaskStatusById(
//                new TaskChangeStatusByIdRequest(token, taskBefore.getId(), Status.IN_PROGRESS));
//        Assert.assertNotNull(response);
//        @Nullable Task Task = response.getTask();
//        Assert.assertNotEquals(taskBefore.getStatus(), Task.getStatus());
//    }
//
//    @Test
//    public void changeTaskStatusByIndex() {
//        Assert.assertThrows(Exception.class,
//                () -> taskEndpoint.changeTaskStatusByIndex(
//                        new TaskChangeStatusByIndexRequest(null, 0, Status.IN_PROGRESS)));
//        Assert.assertThrows(Exception.class,
//                () -> taskEndpoint.changeTaskStatusByIndex(
//                        new TaskChangeStatusByIndexRequest(token, 0, null)));
//        Assert.assertThrows(Exception.class,
//                () -> taskEndpoint.changeTaskStatusByIndex(
//                        new TaskChangeStatusByIndexRequest(token, -1, Status.IN_PROGRESS)));
//        TaskChangeStatusByIndexResponse response = taskEndpoint.changeTaskStatusByIndex(
//                new TaskChangeStatusByIndexRequest(token, 0, Status.IN_PROGRESS));
//        Assert.assertNotNull(response);
//        @Nullable Task Task = response.getTask();
//        Assert.assertNotEquals(taskBefore.getStatus(), Task.getStatus());
//    }
//
//    @Test
//    public void createTask() {
//        Assert.assertThrows(Exception.class,
//                () -> taskEndpoint.createTask(
//                        new TaskCreateRequest(null, "", "", null, null)));
//        Assert.assertThrows(Exception.class,
//                () -> taskEndpoint.createTask(
//                        new TaskCreateRequest(token, "", "", null, null)));
//        @NotNull final String TaskName = "name";
//        @NotNull final Date dateBegin = DateUtil.toDate("10.10.2021");
//        @NotNull final Date dateEnd = DateUtil.toDate("11.11.2021");
//        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(
//                new TaskCreateRequest(token, "name", "description", dateBegin, dateEnd));
//        Assert.assertNotNull(response);
//        @Nullable Task Task = response.getTask();
//        Assert.assertEquals(TaskName, Task.getName());
//    }
//
//    @Test
//    public void removeTaskByIdTest() {
//        Assert.assertThrows(Exception.class,
//                () -> taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(null, taskBefore.getId())));
//        taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(token, taskBefore.getId()));
//        Assert.assertNull(taskEndpoint.listTaskAll(new TaskListRequest(token, null)).getTasks());
//    }
//
//    @Test
//    public void removeTaskByIndexTest() {
//        Assert.assertThrows(Exception.class,
//                () -> taskEndpoint.removeTaskByIndex(new TaskRemoveByIndexRequest(null, 0)));
//        Assert.assertThrows(Exception.class,
//                () -> taskEndpoint.removeTaskByIndex(new TaskRemoveByIndexRequest(token, -1)));
//        taskEndpoint.removeTaskByIndex(new TaskRemoveByIndexRequest(token, 0));
//        Assert.assertNull(taskEndpoint.listTaskAll(new TaskListRequest(token, null)).getTasks());
//    }
//
//    @Test
//    public void showTaskById() {
//        Assert.assertThrows(Exception.class,
//                () -> taskEndpoint.showTaskById(new TaskShowByIdRequest(null, taskBefore.getId())));
//        @NotNull final TaskShowByIdResponse response = taskEndpoint.showTaskById(
//                new TaskShowByIdRequest(token, taskBefore.getId()));
//        Assert.assertNotNull(response);
//        Assert.assertNotNull(response.getTask());
//    }
//
//    @Test
//    public void showTaskByIndex() {
//        Assert.assertThrows(Exception.class,
//                () -> taskEndpoint.showTaskByIndex(new TaskShowByIndexRequest(null, 0)));
//        Assert.assertThrows(Exception.class,
//                () -> taskEndpoint.showTaskByIndex(new TaskShowByIndexRequest(token, -1)));
//        @NotNull final TaskShowByIndexResponse response = taskEndpoint.showTaskByIndex(
//                new TaskShowByIndexRequest(token, 0));
//        Assert.assertNotNull(response);
//        Assert.assertNotNull(response.getTask());
//    }
//
//    @Test
//    public void updateTaskById() {
//        Assert.assertThrows(Exception.class,
//                () -> taskEndpoint.updateTaskById(
//                        new TaskUpdateByIdRequest(null, taskBefore.getId(), "", "")));
//        @NotNull final TaskUpdateByIdResponse response = taskEndpoint.updateTaskById(
//                new TaskUpdateByIdRequest(token, taskBefore.getId(), "new_name", "new_description"));
//        Assert.assertNotNull(response);
//        @Nullable Task Task = response.getTask();
//        Assert.assertNotNull(Task);
//        Assert.assertNotEquals(taskBefore.getName(), Task.getName());
//    }
//
//    @Test
//    public void updateTaskByIndex() {
//        Assert.assertThrows(Exception.class,
//                () -> taskEndpoint.updateTaskByIndex(
//                        new TaskUpdateByIndexRequest(null, 0, "", "")));
//        Assert.assertThrows(Exception.class,
//                () -> taskEndpoint.updateTaskByIndex(
//                        new TaskUpdateByIndexRequest(token, -1, "", "")));
//        @NotNull final TaskUpdateByIndexResponse response = taskEndpoint.updateTaskByIndex(
//                new TaskUpdateByIndexRequest(token, 0, "new_name", "new_description"));
//        Assert.assertNotNull(response);
//        @Nullable Task Task = response.getTask();
//        Assert.assertNotNull(Task);
//        Assert.assertNotEquals(taskBefore.getName(), Task.getName());
//    }
//
//}